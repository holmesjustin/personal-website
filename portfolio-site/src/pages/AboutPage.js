import React from 'react';

import Hero from "../components/Hero";
import Content from '../components/Content';

import Composite from '../images/Composite.JPG'

function AboutPage(props) {

	return(
		<div>
			<Hero title={props.title} subTitle={props.subTitle} />

			<img className="about-me-photo" src={Composite} alt={Composite} />

			<Content>
				<p>Hello! My name is Justin Holmes, and I am a junior at Duke University.</p>
				<p>This is a little about myself...</p>
				<p>I started <a href="https://www.holmesaerialphotography.com" target="_blank" rel="noopener noreferrer">Holmes Aerial Photography</a> which is cool.</p>
			</Content>
		</div>
	);
}


export default AboutPage;