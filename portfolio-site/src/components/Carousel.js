import React from 'react';
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'


import Card from "./Card";


import source1 from "../images/Google.jpg";
import source2 from "../images/Bing.jpg";
import source3 from "../images/Yahoo.png";

class Carousel extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			items: [
				{
					id: 0,
					title: "Example 1",
					subTitle: "Example 1 Description",
					imgSrc: source1,
					link: "https://www.google.com",
					selected: false
				},
				{
					id: 1,
					title: "Example 2",
					subTitle: "Example 2 Description",
					imgSrc: source2,
					link: "https://www.bing.com",
					selected: false
				},
				{
					id: 2,
					title: "Example 3",
					subTitle: "Example 3 Description",
					imgSrc: source3,
					link: "https://www.yahoo.com",
					selected: false
				}

			]

		}

	}


	handleCardClick = (id, card) => {

		let items = [...this.state.items];

		items[id].selected = items[id].selected ? false : true;

		items.forEach(item => {
			if(item.id !== id) {
				item.selected = false;
			} 
		});

		this.setState({
			items
		});

	}


	makeItems = (items) => {
		return items.map(item => {
			return <Card item ={item} click={(e => this.handleCardClick(item.id, e))} key={item.id} />
		});

	}



	render() {
		return(

			<Container fluid={true}>
				<Row className='justify-content-around'>
					{this.makeItems(this.state.items)}
				</Row>
			</Container>

		);
	}
}


export default Carousel;