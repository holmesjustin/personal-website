import React from 'react';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';


function Hero(props) {

	return(

		<Jumbotron className="bg-transparent jumbotron-fluid p-0">
			<Container fluid={true}>
				<Row className="text-center py-2">
					<Col md={12} sm = {16}>
						{ props.title && <h1 className="display-3 font-weight-bolder">{props.title}</h1> }
						{ props.subTitle && <h3 className="display-7 font-weight-light">{props.subTitle}</h3> }
					</Col>
				</Row>
			</Container>
		</Jumbotron>

	);

}


export default Hero;